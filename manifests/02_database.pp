class { '::mysql::server':
    root_password  => 'smartlab',
    override_options => {
        mysqld => {
            'bind_address' => '0.0.0.0',
        }
    }
}
mysql::db { 'smartlab':
    user     => 'smartlab',
    password => 'smartlab',
    host     => '%',
    charset  => 'utf8',
}

class {'::mongodb::server':
    port    => 27017,
}

mongodb::db { 'smartlab':
    user => 'smartlab',
    password => 'smartlab',
}
