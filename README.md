SmartController Web App
=======================

Welcome to the ECE PPE Project: SmartController Web App
This appliance lets you install the project in a Virtual Environment (Virtualbox or VMware) by using the DevOps techniques.

Project Requeriments
--------------------

Tot install the SmartController Web App, the following requirements are needed:

  * Linux or Windows machine with Vagrant
  * Virtualbox (recommended) or VMware machine with 20GB of Disk with 1GB of RAM

  ````
sudo apt-get install virtualbox 
sudo apt-get install vagrant
sudo apt-get install virtualbox-dkms
  ````

Downloading the latest relase of the SmartController Web App
------------------------------------------------------------

  * Downloading the project from Git repo
  ````  
git clone git clone https://<username>@bitbucket.org/pabloskius25/eceproject.git eceproject
  ````

Deploying the Appliance for the first time
------------------------------------------

  * Navigate to the eceproject source directory.
  ````
vagrant deploy
  ````

  * Starting an already deployed appliance from the source directory
  ````
vagrant up
  ````

  * Suspending the virtual appliance
  ````
vagrant suspend
  ````

Contributors
------------

Albert Terrones
Pau Madrero
Oriol Fort
Héctor Arreola

License
-------
This project is provided as LGPL3 and can be modified and redistribute without restrictions.