<?php
namespace CAMINS\labBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 *  @MongoDB\EmbeddedDocument
 */
class DeviceData {

  public function __construct()
  {
    $this->server_time = new \MongoDate(time());
  }

  /**
   * @MongoDB\String
   */
  protected $jsondata;

  /**
   * @MongoDB\date
   */
  protected $server_time;


  /**
   * Get jsondata
   *
   * @return string $jsondata
   */
  public function getJsondata()
  {
      return $this->jsondata;
  }

  /**
   * Set jsondata
   *
   * @param string $jsondata
   * @return self
   */
  public function setJsondata($jsondata)
  {
      $this->jsondata = $jsondata;
      return $this;
  }

  public function __toString()
  {
    return (string)$this->jsondata;
  }

}
