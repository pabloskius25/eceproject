<?php
namespace CAMINS\labBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @MongoDB\Document
 */
class DeviceDocument
{
    /**
     * @MongoDB\Id
     */
    protected $id;


    /**
     * @MongoDB\Integer
     */
    protected $deviceId;

    /**
     * @MongoDB\Integer
     */
    protected $experimentId;

    /**
    * @MongoDB\EmbedMany(targetDocument="DeviceData")
    */
    protected $deviceData;

    public function __construct () {
        $this->deviceData = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deviceId
     *
     * @param int $deviceId
     * @return self
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;
        return $this;
    }

    /**
     * Get deviceId
     *
     * @return int $deviceId
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * Get experimentId
     *
     * @return int $experimentId
     */
    public function getExperimentId()
    {
        return $this->experimentId;
    }

    /**
     * Set experimentId
     *
     * @param int $experimentId
     * @return self
     */
    public function setExperimentId($experimentId)
    {
        $this->experimentId = $experimentId;
        return $this;
    }

    /**
    * Add deviceData
    *
    * @param \CAMINS\labBundle\Document\DeviceData $deviceData
    * @return DeviceDocument
    */
    public function addDeviceData(\CAMINS\labBundle\Document\DeviceData $deviceData)
    {
      $this->deviceData[] = $deviceData;
      return $this;
    }

    /**
    * Remove deviceData
    *
    * @param \CAMINS\labBundle\Document\DeviceData $deviceData
    */
    public function removeDeviceData(\CAMINS\labBundle\Document\DeviceData $deviceData)
    {
      $this->deviceData->removeElement($deviceData);
    }

    /**
    * Get deviceData
    *
    * @return \Doctrine\Common\Collections\Collection
    */
    public function getDeviceData()
    {
      return $this->deviceData;
    }

}
