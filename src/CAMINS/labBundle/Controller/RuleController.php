<?php
namespace CAMINS\labBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\StreamedResponse;

use CAMINS\labBundle\Entity\Rule;
use CAMINS\labBundle\Form\RuleType;

/**
 * Rule controller.
 */
class RuleController extends Controller
{
    const INDEX  = 'rule_index';
    const VIEW   = 'rule_view';
    const CREATE = 'rule_create';
    const UPDATE = 'rule_update';
    const DELETE = 'rule_delete';

    private $ruleService = null;
    private $deviceService = null;

    public function init()
    {
        $this->ruleService = $this->get('rule_service');
        $this->deviceService = $this->get('device_service');
    }

    /**
     * Create a new Rule entity.
     *
     * @Route("/rule/create", name="rule_create")
     * @Method({"GET","POST"})
     */
     public function createAction(Request $request)
     {
         $this->init();

         $rule = $this->ruleService->createRule();
         $form = $this->createForm(RuleType::class, $rule);
         $form->add('submit', 'submit', array('label' => ($rule->getId() == null ? 'Create' : 'Update'), 'attr' => array('class' =>'btn btn-default')));
         $form->handleRequest($request);

         if ($form->isValid()) {
             $this->ruleService->saveRule($rule);
         }

		 if ($template = $this->getFormTemplate($request, $rule))
					 return $this->render('CAMINSlabBundle:Rule:create.html.twig', $template);
		
		 return $this->redirectToRoute(self::INDEX, array());
			 
         return $this->render(
             'CAMINSlabBundle:Rule:create.html.twig',
             array('form' => $form->createView())
         );

     }

     /**
      * Update an existing Rule entity.
      *
      * @Route("/rule/{id}/update", name="rule_update")
      * @Method({"GET", "PUT", "POST"})
      */
     public function updateAction(Request $request, $id)
     {
         $this->init();
         $rule = $this->ruleService->getRule($id);
         if (!$rule) {
             throw new NotFoundHttpException();
          }
         $this->denyAccessUnlessGranted(self::UPDATE, $rule);
         if ($template = $this->getFormTemplate($request, $rule))
             return $this->render('CAMINSlabBundle:Rule:update.html.twig', $template);
         return $this->redirectToRoute(self::INDEX, array());
     }

     /**
      * Deletes an Rule entity.
      *
      * @Route("/rule/{id}/delete", name="rule_delete")
      * @Method("GET")
      */
     public function deleteAction(Request $request, $id)
     {
         $this->init();

         $rule = $this->ruleService->getRule($id);
         if (!$rule) {
             throw new NotFoundHttpException();
         }
         $this->denyAccessUnlessGranted(self::DELETE, $rule);
         $this->ruleService->deleteRule($rule);
         return $this->redirectToRoute(self::INDEX, array());
     }

     /**
      * Displays an Rule entity.
      *
      * @Route("/rule/{id}", name="rule_view")
      * @Method("GET")
      */
     public function viewAction($id)
     {
         $this->init();
         $rule = $this->ruleService->getRule($id);
         if (!$rule) {
             throw new NotFoundHttpException();
         }
         $this->denyAccessUnlessGranted(self::VIEW, $rule);
         $template = array(
             'rule' => $rule,
         );
         return $this->render('CAMINSlabBundle:Rule:view.html.twig', $template);
     }

    /**
     * Lists all Experiments entities by Laboratory.
     *
     * @Route("/rule/", name="rule_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $this->init();
        $rules = $this->ruleService->getRules();
        $template = array(
            'rules' => $rules,
        );
        return $this->render('CAMINSlabBundle:Rule:index.html.twig', $template);
    }

     /**
      * Exports a Rule DocumentStore.
      *
      * @Route("/rule/{id}/download", name="rule_download")
      * @Method("GET")
      */
    public function downloadAction($id)
    {
        $this->init();
        $rule = $this->ruleService->getRule($id);
        $response = new StreamedResponse();
        $response->setCallback(function() use ($rule){
              $handle = fopen('php://output', 'w+');
              $ruleData = $this->ruleService->getRuleWithData($rule->getId())->tempData->toArray();
              $firstLineKeys = false;
              foreach ($ruleData as $item)
              {
                  $line = json_decode($item, true);
                  if (empty($firstLineKeys))
                  {
                      $firstLineKeys = array_keys($line);
                      fputcsv($handle, $firstLineKeys);
                      $firstLineKeys = array_flip($firstLineKeys);
                  }
                  $line_array = array();
                  foreach ($line as $value)
                  {
                      array_push($line_array,$value);
                  }
                  fputcsv($handle, $line_array);

              }
              fclose($handle);
          });
          $response->setStatusCode(200);
          $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
          $response->headers->set('Content-Disposition','attachment; filename="rule.csv"');
          return $response;
    }

    private function getFormTemplate(Request $request, Rule $rule)
    {
        if ($rule->getId()) {
            $action = $this->generateUrl(self::UPDATE, array('id' => $rule->getId()));
        }
        else {
            $action = $this->generateUrl(self::CREATE, array());
        }

        $form = $this->createForm(new RuleType($this->getDoctrine()), $rule, array(
            'action' => $action,
            'attr' => array('class' => 'form-edit'),
            'method' => ($rule->getId() == null ? 'POST' : 'PUT'),
        ));
        $form->add('submit', 'submit', array('label' => ($rule->getId() == null ? 'Create' : 'Update'), 'attr' => array('class' =>'btn btn-default')));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->ruleService->saveRule($rule, $this->getUser());
            return false;
        }
        return array(
            'form' => $form->createView(),
        );
    }
}
