<?php
namespace CAMINS\labBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\StreamedResponse;

use CAMINS\labBundle\Entity\Device;
use CAMINS\labBundle\Form\DeviceType;

/**
 * Device controller.
 */
class DeviceController extends Controller
{
    const INDEX  = 'device_index';
    const VIEW   = 'device_view';
    const DOWNLOAD  = 'device_download';
    const CREATE = 'device_create';
    const UPDATE = 'device_update';
    const DELETE = 'device_delete';

    private $deviceService = null;

    public function init()
    {
        $this->deviceService = $this->get('device_service');
    }

    /**
     * Create a new Device entity.
     *
     * @Route("/device/create", name="device_create")
     * @Method({"GET","POST"})
     */
     public function createAction(Request $request)
     {
         $this->init();
         $device = $this->deviceService->createDevice();
         $this->denyAccessUnlessGranted(self::CREATE, $device);
         if ($template = $this->getFormTemplate($request, $device))
             return $this->render('CAMINSlabBundle:Device:create.html.twig', $template);
         return $this->redirectToRoute(self::INDEX, array());
     }

     /**
      * Update an existing Device entity.
      *
      * @Route("/device/{id}/update", name="device_update")
      * @Method({"GET", "PUT"})
      */
     public function updateAction(Request $request, $id)
     {
         $this->init();
         $device = $this->deviceService->getDevice($id);
         if (!$device) {
             throw new NotFoundHttpException();
         }
         $this->denyAccessUnlessGranted(self::UPDATE, $device);
         if ($template = $this->getFormTemplate($request, $device))
             return $this->render('CAMINSlabBundle:Device:update.html.twig', $template);
         return $this->redirectToRoute(self::INDEX, array());
     }

     /**
      * Deletes an Device entity.
      *
      * @Route("/device/{id}/delete", name="device_delete")
      * @Method("GET")
      */
     public function deleteAction(Request $request, $id)
     {
         $this->init();

         $device = $this->deviceService->getDevice($id);
         $device->deleteAllSensorActuator();
         $this->deviceService->saveDevice($device, $this->getUser());
         if (!$device) {
             throw new NotFoundHttpException();
         }
         $this->denyAccessUnlessGranted(self::DELETE, $device);
         $this->deviceService->deleteDevice($device);
         return $this->redirectToRoute(self::INDEX, array());
     }

     /**
      * Displays an Device entity.
      *
      * @Route("/device/{id}", name="device_view")
      * @Method("GET")
      */
     public function viewAction($id)
     {
         $this->init();
         $device = $this->deviceService->getDevice($id);
         if (!$device) {
             throw new NotFoundHttpException();
         }
         $this->denyAccessUnlessGranted(self::VIEW, $device);
         $template = array(
             'device' => $device,
         );
         return $this->render('CAMINSlabBundle:Device:view.html.twig', $template);
     }

    /**
     * Lists all Experiments entities by Laboratory.
     *
     * @Route("/device/", name="device_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $this->init();
        $devices = $this->deviceService->getDevices();
        $template = array(
            'devices' => $devices,
        );
        return $this->render('CAMINSlabBundle:Device:index.html.twig', $template);
    }

     /**
      * Exports a Device DocumentStore.
      *
      * @Route("/device/{id}/download", name="device_download")
      * @Method("GET")
      */
    public function downloadAction($id)
    {
        $this->init();
        $device = $this->deviceService->getDevice($id);
        $response = new StreamedResponse();
        $response->setCallback(function() use ($device){
              $handle = fopen('php://output', 'w+');
              $deviceData = $this->deviceService->getDeviceWithData($device->getId())->tempData->toArray();
              $firstLineKeys = false;
              foreach ($deviceData as $item)
              {
                  $line = json_decode($item, true);
                  if (empty($firstLineKeys))
                  {
                      $firstLineKeys = array_keys($line);
                      fputcsv($handle, $firstLineKeys);
                      $firstLineKeys = array_flip($firstLineKeys);
                  }
                  $line_array = array();
                  foreach ($line as $value)
                  {
                      array_push($line_array,$value);
                  }
                  fputcsv($handle, $line_array);

              }
              fclose($handle);
          });
          $response->setStatusCode(200);
          $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
          $response->headers->set('Content-Disposition','attachment; filename="device.csv"');
          return $response;
    }

    private function getFormTemplate(Request $request, Device $device)
    {
        if ($device->getId()) {
            $action = $this->generateUrl(self::UPDATE, array('id' => $device->getId()));
        }
        else {
            $action = $this->generateUrl(self::CREATE, array());
        }

        $form = $this->createForm(new DeviceType($this->getDoctrine()), $device, array(
            'action' => $action,
            'attr' => array('class' => 'form-edit'),
            'method' => ($device->getId() == null ? 'POST' : 'PUT'),
        ));
        $form->add('submit', 'submit', array('label' => ($device->getId() == null ? 'Create' : 'Update'), 'attr' => array('class' =>'btn btn-default')));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->deviceService->saveDevice($device, $this->getUser());
            return false;
        }
        return array(
            'form' => $form->createView(),
        );
    }
}
