<?php

namespace CAMINS\labBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CAMINS\labBundle\Entity\Sensor;
use CAMINS\labBundle\Form\SensorType;

/**
 * Sensor controller.
 *
 * @Route("/sensor")
 */
class SensorController extends Controller
{

    /**
     * Lists all Sensor entities.
     *
     * @Route("/", name="sensor_home")
     * @Method("GET")
     */
    public function indexAction()
    {
        $sensorService = $this->get("sensor_service");
        $sensors = $sensorService->getAllSensors();

        return $this->render('CAMINSlabBundle:Sensor:index.html.twig',$sensors);
    }

    /**
     * Creates a new Sensor entity.
     *
     * @Route("/", name="sensor_create")
     * @Method("POST")
     * @Template("CAMINSlabBundle:Sensor:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Sensor();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $sensorService = $this->get("sensor_service");
            $sensorService->createSensor($entity);

            return $this->redirect($this->generateUrl('sensor_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Sensor entity.
     *
     * @param Sensor $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Sensor $entity)
    {
        $form = $this->createForm(new SensorType(), $entity, array(
            'action' => $this->generateUrl('sensor_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Sensor entity.
     *
     * @Route("/new", name="sensor_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Sensor();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Sensor entity.
     *
     * @Route("/{id}", name="sensor_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CAMINSlabBundle:Sensor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sensor entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Sensor entity.
     *
     * @Route("/{id}/edit", name="sensor_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CAMINSlabBundle:Sensor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sensor entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Sensor entity.
    *
    * @param Sensor $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Sensor $entity)
    {
        $form = $this->createForm(new SensorType(), $entity, array(
            'action' => $this->generateUrl('sensor_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Sensor entity.
     *
     * @Route("/{id}", name="sensor_update")
     * @Method("PUT")
     * @Template("CAMINSlabBundle:Sensor:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CAMINSlabBundle:Sensor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sensor entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('sensor_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Sensor entity.
     *
     * @Route("/{id}", name="sensor_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CAMINSlabBundle:Sensor')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Sensor entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('sensor'));
    }

    /**
     * Creates a form to delete a Sensor entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sensor_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
