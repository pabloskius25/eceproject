<?php
namespace CAMINS\labBundle\Controller;

use CAMINS\labBundle\Entity\Device;
use CAMINS\labBundle\Document\DeviceDocument;
use CAMINS\labBundle\Form\DeviceType;
use CAMINS\labBundle\Entity\Sensor;
use CAMINS\labBundle\Entity\Actuator;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Voryx\RESTGeneratorBundle\Controller\VoryxController;

/**
 * Device controller.
 * @RouteResource("Device")
 */
class DeviceRESTController extends VoryxController
{
    private $deviceService = null;
    private $ruleService = null;

    public function init()
    {
        $this->deviceService = $this->get('device_service');
        //$this->ruleService = $this->get('rules_service');
    }

    /**
     * Get a Device entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getAction(Device $entity)
    {
        return $entity;
    }

    /**
     * Get all Device entities.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
        try {
            $offset = $paramFetcher->get('offset');
            $limit = $paramFetcher->get('limit');
            $order_by = $paramFetcher->get('order_by');
            $filters = !is_null($paramFetcher->get('filters')) ? $paramFetcher->get('filters') : array();

            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository('CAMINSlabBundle:Device')->findBy($filters, $order_by, $limit, $offset);
            if ($entities) {
                return $entities;
            }

            return FOSView::create('Not Found', Codes::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Create a Device entity.
     *
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function postAction(Request $request)
    {
		$em = $this->getDoctrine()->getManager();
		
		if ($this->get('request')->request->get('idDevice')) {
			$entity = $em->getRepository('CAMINSlabBundle:Device')->findOneById($this->get('request')->request->get('idDevice')); 
			$entity->setFullname($this->get('request')->request->get('fullname'));
			$entity->setToken($this->get('request')->request->get('token'));
			$entity->setUpdateTime(new \DateTime());
			
			$temporarySensors = explode(",",$this->get('request')->request->get('temporarySensors'));
			$temporaryActuators = explode(",",$this->get('request')->request->get('temporaryActuators'));
	
			$this->updateSensors($em, $entity, $entity->getSensors(), $temporarySensors);
			$this->updateItems($em, $entity, $entity->getActuators(), $temporaryActuators);
			$em->persist($entity);
			$em->flush();
				
			return $entity;
			
		} else {
			$entity = new Device();
			$form = $this->createForm(new DeviceType(), $entity, array("method" => $request->getMethod()));     

			$this->removeExtraFields($request, $form);
			$form->handleRequest($request);

			if ($form->isValid()) {
				$em = $this->getDoctrine()->getManager();
				$temporarySensors = explode(",",$entity->getTemporarySensors());
				$temporaryActuators = explode(",",$entity->getTemporaryActuators());

				$entity->setCreateDateTime(new \DateTime());
				$entity->setUpdateTime(new \DateTime());

				foreach ($temporarySensors as $sensorNameUnit) {
				  $sensorPair = explode(":",$sensorNameUnit);
				  $sensor = new Sensor();
				  $sensor->setName($sensorPair[0]);
				  $sensor->setUnit($sensorPair[1]);
				  $entity->addSensor($sensor);
				  $sensor->setDevice($entity);
				  $em->persist($sensor);
				}

				foreach ($temporaryActuators as $actuatorName) {
				  $actuator = new Actuator();
				  $actuator->setName($actuatorName);
				  $entity->addActuator($actuator);
				  $actuator->setDevice($entity);
				  $em->persist($actuator);
				}
				$em->persist($entity);
				$em->flush();

				return $entity;
			}			
		}

        return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }
    
    private function updateItems($em, &$device, $curr_items, $new_items) {

		foreach($curr_items as $item) {
			if (!in_array($item->getName(), $new_items)) {
				if ($item instanceof Sensor) {
					$device->removeSensor($item);
				} else if ($item instanceof Actuator) {
					$device->removeActuator($item);
				}
				$em->remove($item);
				$em->persist($device);
				$em->flush();
			}
		}
		$em->flush();		
		foreach ($new_items as $new_item) {
			$found = false;
			foreach($curr_items as $curr_item) {
				if ($curr_item->getName() == $new_item) {
					$found = true;
					break;
				}
			}
			
			if (!$found) {				
				if ($new_item instanceof Sensor) {
				  $sensor = new Sensor();	
				  $sensor->setName($new_item);
				  $device->addSensor($sensor);
				  $sensor->setDevice($device);
				  $em->persist($sensor);				   
				}			

				if ($new_item instanceof Actuator) {
				  $actuator = new Actuator();
				  $actuator->setName($new_item);
				  $device->addActuator($actuator);
				  $actuator->setDevice($device);
				  $em->persist($actuator);			   
				}				
			}		
		}
		
		$em->persist($device);
		$em->flush();
	}
	
	private function updateSensors($em, &$device, $curr_items, $new_items) {
		foreach($new_items as $pair) {
			$sensorUnit = explode(":",$pair);
			$keyValueItems[$sensorUnit[0]] = $sensorUnit[1];
		}


		foreach($curr_items as $item) {
			if (!array_key_exists($item->getName(), $keyValueItems)) {
				$device->removeSensor($item);
				$em->remove($item);
				$em->persist($device);
				$em->flush();
			}
		}
		$em->flush();		
		foreach ($keyValueItems as $new_item_key => $new_item_unit) {
			$found = false;
			foreach($curr_items as $curr_item) {
				if ($curr_item->getName() == $new_item_key) {
					$found = true;
					break;
				}
			}
			
			if (!$found) {				
			  $sensor = new Sensor();	
			  $sensor->setName($new_item_key);
			  $sensor->setUnit($new_item_unit);
			  $device->addSensor($sensor);
			  $sensor->setDevice($device);
			  $em->persist($sensor);			
			}		
		}
		
		$em->persist($device);
		$em->flush();
	}

    /**
     * Update a DeviceDocument entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $device
     *
     * @return Response
     */
    public function putAction(Request $request, Device $entity)
    {
        $this->init();
        $em = $this->getDoctrine()->getManager();
        try {
            $providedData = $this->get('request')->request->get('data');
            $jsonData = json_decode($providedData, true);

            foreach($entity->getSensors() as $sensor) {
                $sensor->setLastValue($jsonData[$sensor->getName()]);
                $em->persist($sensor);
            }
			
			$affectedDevices = array();
			foreach($entity->getSensors() as $sensor) {
				foreach ($sensor->getConditionals() as $conditional) {
					$affectedDevices[] = $conditional->getRule()->getActuator()->getDevice();								
				}
			}
			
			foreach(array_unique($affectedDevices) as $device) {
				$resultActuators = array();
				foreach ($device->getActuators() as $actuator){
					$resultActuators[$actuator->getName()] = false;
					foreach ($actuator->getRules() as $rule){
						$activate = true;
						foreach ($rule->getConditionals() as $conditional) {
							$sensor = $conditional->getSensor();
							$lastValue = $sensor->getLastValue();
							$comparator = $conditional->getComparator();
							$constant = $conditional->getConstant();
							if($lastValue === null) return;
							
							if(!eval("return $lastValue $comparator $constant ;")) {
								$activate = false;
								break;
							}
						}
						if($activate) {
							$resultActuators[$actuator->getName()] = true;
							break;
						}
					}
				}	
				$post_data = json_encode($resultActuators,JSON_HEX_QUOT);
				$send = false;			
				foreach($device->getActuators() as $actu) {
					
					if ($resultActuators[$actu->getName()] != $actu->getLastValue()) {
						$send = true;
						$actu->setLastValue($resultActuators[$actu->getName()]);
						$em->persist($actu);
					} 
					
				}
				if ($send) {
					$this->sendPushNotification($device->getToken(), $post_data);	
				}
					
				$em->flush();								
				return $post_data;
			}

        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    // function makes curl request to gcm servers
    private function sendPushNotification($deviceToken, $data) {
          $fields = array(
              'to' => $deviceToken,
              'data' => array('message' => $data),
          );

          // Set POST variables
          $url = 'https://gcm-http.googleapis.com/gcm/send';

          $headers = array(
              //'Authorization: key=' .'⁠⁠⁠AIzaSyBRvkfSWL6P9VP7fCNzeMftlWhJzYCcwLE',
              'Authorization: key=' .'AIzaSyAiZlBOqS4VCmBCMW1egerqqmYMDXJffW8',
              'Content-Type: application/json'
          );

          //var_dump($headers);
          // Open connection
          $ch = curl_init();

          // Set the url, number of POST vars, POST data
          curl_setopt($ch, CURLOPT_URL, $url);

          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

          // Disabling SSL Certificate support temporarly
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

          curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

          // Execute post
          $result = curl_exec($ch);
          if ($result === FALSE) {
              die('Curl failed: ' . curl_error($ch));
          }
          //var_dump($result);

          // Close connection
          curl_close($ch);

          return $result;
      }

}

