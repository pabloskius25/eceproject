<?php

namespace CAMINS\labBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CAMINS\labBundle\Entity\Conditional;
use CAMINS\labBundle\Form\ConditionalType;

/**
 * Conditional controller.
 */
class ConditionalController extends Controller
{

    const INDEX  = 'conditional_index';
    const VIEW   = 'conditional_view';
    const CREATE = 'conditional_create';
    const UPDATE = 'conditional_update';
    const DELETE = 'conditional_delete';

    private $ruleService = null;
    private $deviceService = null;

    public function init()
    {
        $this->ruleService = $this->get('rule_service');
        $this->deviceService = $this->get('device_service');
    }

     /**
      * Lists all Conditionals entities by Rule.
      *
      * @Route("/rule/{ruleId}/conditional", name="conditional_index")
      * @Method("GET")
      * @Template()
      */
    public function indexAction($ruleId)
    {
        $em = $this->getDoctrine()->getManager();

         $this->init();
         $rule = $this->ruleService->getRule($ruleId);

        //$entities = $em->getRepository('CAMINSlabBundle:Conditional')->findAll();
        $entities = $rule->getConditionals();;

        return array(
            'entities' => $entities,
            'rule' => $rule
        );
    }
    /**
     * Creates a new Conditional entity.
     *
     * @Route("/", name="conditional_create")
     * @Method("POST")
     * @Template("CAMINSlabBundle:Conditional:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Conditional();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
			/*
			$this->init();
			$rule = $this->ruleService->getRule($ruleId);
			$em->setRule($rule);			
			*/
			
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('conditional_index', array('ruleId' => $entity->getRule()->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Conditional entity.
     *
     * @param Conditional $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Conditional $entity)
    {
        $form = $this->createForm(new ConditionalType(), $entity, array(
            'action' => $this->generateUrl('conditional_create'),
            'method' => 'POST',
            'attr' => array('class' => 'form-edit'),
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

     /**
     * Displays a form to create a new Conditional entity.
      *
      * @Route("/rule/{ruleId}/conditional/new", name="conditional_new")
      * @Method("GET")
      * @Template()
      */
    public function newAction($ruleId)
    {
        $entity = new Conditional();
		$this->init();
		$rule = $this->ruleService->getRule($ruleId);
		$entity->setRule($rule);
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'rule' => $rule,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Conditional entity.
     *
     * @Route("/{id}", name="conditional_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CAMINSlabBundle:Conditional')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Conditional entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Conditional entity.
     *
     * @Route("/{id}/edit", name="conditional_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CAMINSlabBundle:Conditional')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Conditional entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Conditional entity.
    *
    * @param Conditional $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Conditional $entity)
    {
        $form = $this->createForm(new ConditionalType(), $entity, array(
            'action' => $this->generateUrl('conditional_update', array('id' => $entity->getId())),
            'attr' => array('class' => 'form-edit'),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Conditional entity.
     *
     * @Route("/{id}", name="conditional_update")
     * @Method("PUT")
     * @Template("CAMINSlabBundle:Conditional:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CAMINSlabBundle:Conditional')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Conditional entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('conditional_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Conditional entity.
     *
	 * @Route("/rule/{ruleId}/conditional/{id}/delete", name="conditional_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request,$ruleId, $id)
    {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('CAMINSlabBundle:Conditional')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Conditional entity.');
		}
		$em->remove($entity);
		$em->flush();
	
        return $this->redirect($this->generateUrl('conditional_index', array('ruleId' => $ruleId)));
    }
}
