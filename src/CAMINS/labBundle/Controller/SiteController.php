<?php
namespace CAMINS\labBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Site controller.
 *
 * @Route("/")
 */
class SiteController extends Controller
{
    /**
     * @Route(name="site_index")
     */
    public function indexAction()
    {
        $user = $this->getUser();
        if ($user) {
            return $this->redirect($this->generateUrl('rule_index'));
        }
        return $this->render('CAMINSlabBundle:Site:index.html.twig');
    }

    /**
     * @Route("/help", name="site_help")
     */
    public function helpAction()
    {
        return $this->render('CAMINSlabBundle:Site:help.html.twig');
    }
}
