<?php

namespace CAMINS\labBundle\Controller;

use CAMINS\labBundle\Entity\Rule;
use CAMINS\labBundle\Form\RuleRESTType;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Voryx\RESTGeneratorBundle\Controller\VoryxController;

/**
 * Rule controller.
 * @RouteResource("Rule")
 */
class RuleRESTController extends VoryxController
{
    /**
     * Get a Rule entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getAction(Rule $entity)
    {
        return $entity;
    }
    /**
     * Get all Rule entities.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
        try {
            $offset = $paramFetcher->get('offset');
            $limit = $paramFetcher->get('limit');
            $order_by = $paramFetcher->get('order_by');
            $filters = !is_null($paramFetcher->get('filters')) ? $paramFetcher->get('filters') : array();

            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository('CAMINSlabBundle:Rule')->findBy($filters, $order_by, $limit, $offset);
            if ($entities) {
                return $entities;
            }

            return FOSView::create('Not Found', Codes::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Create a Rule entity.
     *
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function postAction(Request $request)
    {
        $entity = new Rule();
		$providedData = $this->get('request')->request->get('data');
		$jsonData = json_decode($providedData, true);

        $em = $this->getDoctrine()->getManager();
        $actuator = $em->getRepository('CAMINSlabBundle:Actuator')->find($jsonData['actuator']);        

		$entity->setFullname($jsonData['fullname']);
		$entity->setActuator($actuator);
        $em->persist($entity);
        $em->flush();
		
		return $entity;
    }
    /**
     * Update a Rule entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function putAction(Request $request, Rule $entity)
    {
		$providedData = $this->get('request')->request->get('data');
		$jsonData = json_decode($providedData, true);

        $em = $this->getDoctrine()->getManager();
        $actuator = $em->getRepository('CAMINSlabBundle:Actuator')->find($jsonData['actuator']);        

		$entity->setFullname($jsonData['fullname']);
		$entity->setActuator($actuator);
        $em->persist($entity);
        $em->flush();
		
		return $entity;

    }
    /**
     * Partial Update to a Rule entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function patchAction(Request $request, Rule $entity)
    {
        return $this->putAction($request, $entity);
    }
    /**
     * Delete a Rule entity.
     *
     * @View(statusCode=204)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function deleteAction(Request $request, Rule $entity)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();

            return null;
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
