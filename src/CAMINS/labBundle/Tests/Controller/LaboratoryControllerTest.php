<?php

namespace CAMINS\labBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class LaboratoryControllerTest extends WebTestCase
{
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testCompleteScenario()
    {
      $this->logIn();

      $crawler = $this->client->request('GET', '/login');
      $this->assertTrue($this->client->getResponse()->isSuccessful());
      $this->assertGreaterThan(0, $crawler->filter('html:contains("Username or email")')->count());

      $form = $crawler->selectButton('_submit')->form(array(
        '_username'  => 'albert.terrones',
        '_password'  => 'albert.terrones',
      ));

      $this->client->submit($form);
      $crawler = $this->client->followRedirect();
      $this->assertTrue($this->client->getResponse()->isSuccessful());
      $this->assertGreaterThan(0, $crawler->filter('html:contains("List of Laboratories")')->count());

      // Create a new Laboratory in the database
      $crawler = $this->client->request('GET', '/laboratory/');
      $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /laboratory/");
      $crawler = $this->client->click($crawler->selectLink('Create a new Laboratory')->link());

      // Fill in the form and submit it
      $form = $crawler->selectButton('Create')->form(array(
          'camins_labbundle_laboratory[fullname]'  => 'Full Test Laboratory',
          'camins_labbundle_laboratory[acronym]'  => 'FTL',
          'camins_labbundle_laboratory[specialization]'  => 'Specialization Test',
          'camins_labbundle_laboratory[physicalAddress]'  => 'Test Address 123',
          'camins_labbundle_laboratory[description]'  => 'This is a short description'
      ));

      $this->client->submit($form);
      $crawler = $this->client->followRedirect();

      // Check data in the show view
      $this->assertGreaterThan(0, $crawler->filter('td:contains("Full Test Laboratory")')->count(), 'Missing element td:contains("Full Test Laboratory")');

      // Edit the entity
      $crawler = $this->client->click($crawler->filter('tr:contains("Full Test Laboratory")')->selectLink('Update')->link());

      $form = $crawler->selectButton('Update Laboratory')->form(array(
          'camins_labbundle_laboratory[fullname]'  => 'Full Test Laboratory2'
      ));

      $this->client->submit($form);
      $crawler = $this->client->followRedirect();

      $crawler = $this->client->request('GET', '/laboratory/');

      // Check data in the show view
      $this->assertGreaterThan(0, $crawler->filter('td:contains("Full Test Laboratory2")')->count(), 'Missing element td:contains("Full Test Laboratory2")');

      // Remove the entity
      $crawler = $this->client->click($crawler->filter('tr:contains("Full Test Laboratory2")')->selectLink('Update')->link());
      $form = $crawler->selectButton('Delete Laboratory')->form(array());
      $this->client->submit($form);

      // Check data in the show view
      $this->assertGreaterThan(1, $crawler->filter('td:contains("Full Test Laboratory2")')->count(), 'Laboratory not removed :()');

    }

    private function logIn()
     {
         $session = $this->client->getContainer()->get('session');

         $firewall = 'main';
         $token = new UsernamePasswordToken('albert.terrones', 'albert.terrones', $firewall, array('ROLE_USER'));
         $session->set('_security_'.$firewall, serialize($token));
         $session->save();

         $cookie = new Cookie($session->getName(), $session->getId());
         $this->client->getCookieJar()->set($cookie);
     }


}
