<?php

namespace CAMINS\labBundle\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CAMINS\labBundle\Entity\Sensor;
use CAMINS\labBundle\Form\SensorType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class SensorService {

    private $doctrine;

    function __construct($doctrine)
    {
        $this->doctrine = $doctrine; // Note that this was injected using the arguments in the config.yml
    }

    function getAllSensors($page = 0)
    {
      $em = $this->doctrine;

      $offset = $page * 10;
      $limit = 10;
      //$order_by = $paramFetcher->get('order_by');

      $order_by = null;
      $filters =  array();

      $entities = $em->getRepository('CAMINSlabBundle:Sensor')
                  ->createQueryBuilder('a')
                  ->setMaxResults(10)
                  ->setFirstResult($page*10)
                  ->getQuery()
                  ->getResult();

      $qb = $em->getRepository('CAMINSlabBundle:Sensor')->createQueryBuilder('a');
      $qb->select('count(e.id)');
      $qb->from('CAMINSlabBundle:Sensor','e');
      $count = $qb->getQuery()->getSingleScalarResult();

      $numPagines = 0;
      if ($count > 10) {
        $numPagines = ceil($count/10);
      }

      return array(
        'total_entities' => $count,
        'current_page' => $page,
        'num_pagines' => $numPagines,
        'entities' => $entities,
      );
    }

    function getSensorsFromLaboratory($laboratoryId) {
      $em = $this->doctrine;
      $page = 0;

      $offset = $page * 10;
      $limit = 10;
      //$order_by = $paramFetcher->get('order_by');

      $order_by = null;
      $filters =  array();

      $entities = $em->getRepository('CAMINSlabBundle:Sensor')
                  ->createQueryBuilder('s')
                  ->join('CAMINSlabBundle:Laboratory', 'l')
                  ->where('s.laboratory = :idLaboratory')
                  ->setParameter('idLaboratory', $laboratoryId)
                  ->setMaxResults(10)
                  ->setFirstResult($page*10)
                  ->getQuery()
                  ->getResult();

      return array(
        'total_entities' => 1,
        'current_page' => 1,
        'num_pagines' => 1,
        'entities' => $entities,
      );
    }


    function createSensor(Sensor $Sensor)
    {
      $em = $this->doctrine->getManager();
      $em->persist($Sensor);
      $em->flush();
    }

    function getSensor($id)
    {
      $em = $this->doctrine->getManager();
      $entity = $em->getRepository('CAMINSlabBundle:Sensor')->find($id);

      if (!$entity) {
          throw $this->createNotFoundException('Unable to find Sensor entity.');
      }
      return $entity;
    }

    function saveSensor()
    {
      $em = $this->doctrine->getManager();
      $em->flush();
    }


    function deleteSensor($id){
      $em = $this->doctrine->getManager();
      $entity = $em->getRepository('CAMINSlabBundle:Sensor')->find($id);

      if (!$entity) {
          throw $this->createNotFoundException('Unable to find Sensor entity.');
      }

      $em->remove($entity);
      $em->flush();
    }
}
