<?php
namespace CAMINS\labBundle\Service;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use CAMINS\labBundle\Entity\Rule;
use CAMINS\labBundle\Document\RuleDocument;
use CAMINS\labBundle\Document\RuleData;

class RuleService {

    private $doctrine;

    function __construct($doctrine) {
        $this->doctrine = $doctrine;
    }

    public function getRule($id)
    {
        $em = $this->doctrine->getManager();
        $rule = $em->getRepository('CAMINSlabBundle:Rule')->find($id);
        if (!$rule) {
            throw new NotFoundHttpException();
        }
        return $rule;
    }

    public function getRules()
    {
        $em = $this->doctrine;
        $queryBuilder = $em->getRepository('CAMINSlabBundle:Rule')->createQueryBuilder('r');
        $rules = $queryBuilder->getQuery()->getResult();
        return array(
            'entities' => $rules,
        );
    }

    public function createRule()
    {
        $rule = new Rule();
        return $rule;
    }

    public function saveRule(Rule $rule)
    {
        $em = $this->doctrine->getManager();
        if (!$em->contains($rule)) {
            $em->persist($rule);
            $em->flush();
        }
        $em->persist($rule);
        $em->flush();
    }

    public function deleteRule($rule)
    {
        $em = $this->doctrine->getManager();
        $em->remove($rule);
        $em->flush();
    }
}
