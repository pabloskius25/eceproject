<?php
namespace CAMINS\labBundle\Service;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use CAMINS\labBundle\Entity\Device;
use CAMINS\labBundle\Document\DeviceDocument;
use CAMINS\labBundle\Document\DeviceData;

class DeviceService {

    private $doctrine;
    private $doctrine_mongodb;

    function __construct($doctrine,$doctrine_mongodb) {
        $this->doctrine = $doctrine;
        $this->doctrine_mongodb = $doctrine_mongodb;
    }

    public function getDevice($id)
    {
        $em = $this->doctrine->getManager();
        $device = $em->getRepository('CAMINSlabBundle:Device')->find($id);
        if (!$device) {
            throw new NotFoundHttpException();
        }
        return $device;
    }

    public function getDevices()
    {
        $em = $this->doctrine;
        $queryBuilder = $em->getRepository('CAMINSlabBundle:Device')->createQueryBuilder('r');
        $devices = $queryBuilder->getQuery()->getResult();
        return array(
            'entities' => $devices,
        );
    }

    public function getAllDevices()
    {
        $em = $this->doctrine;
        $queryBuilder = $em->getRepository('CAMINSlabBundle:Device')->createQueryBuilder('r');
        $devices = $queryBuilder->getQuery()->getResult();
        return $devices;
    }

    public function getDeviceWithData($id)
    {
        $device = $this->getDevice($id);
        $dm = $this->doctrine_mongodb->getManager();
        $deviceDocument = $dm->createQueryBuilder('CAMINSlabBundle:DeviceDocument')->field('deviceId')->equals((int)$id)->getQuery()->getSingleResult();
        if (!$deviceDocument) {
            throw new NotFoundHttpException();
        }
        $device->tempData = $deviceDocument->getDeviceData();
        return $device;
    }

    public function createDevice()
    {
        $device = new Device();
        return $device;
    }

    public function saveDeviceData(Device $device, $jsonData)
    {
        $em = $this->doctrine->getManager();
        $dm = $this->doctrine_mongodb->getManager();
        $device->setUpdateTime(new \DateTime());
        $deviceDocument = $dm->createQueryBuilder('CAMINSlabBundle:DeviceDocument')->field('deviceId')->equals($device->getId())->getQuery()->getSingleResult();
        $deviceData = new DeviceData();
        $deviceData->setJsondata($jsonData);
        $deviceDocument->addDeviceData($deviceData);
        $em->flush();
        $dm->flush();
    }

    public function saveDevice(Device $device,$user)
    {
        $em = $this->doctrine->getManager();
        if (!$em->contains($device)) {
            $device->setCreateDateTime(new \DateTime());
            $device->setUpdateTime(new \DateTime());
            $device->setToken();
            $device->setManager($user);
            $em->persist($device);
            $em->flush();
            $deviceDocument = new DeviceDocument();
            $deviceDocument->setDeviceId($device->getId());
            $dm = $this->doctrine_mongodb->getManager();
            $dm->persist($deviceDocument);
            $dm->flush();
        }
        $em->persist($device);
        $em->flush();
    }

    public function deleteDevice($device)
    {
        $em = $this->doctrine->getManager();
        $dm = $this->doctrine_mongodb->getManager();
        $deviceDocument = $dm->createQueryBuilder('CAMINSlabBundle:DeviceDocument')->field('deviceId')->equals($device->getId())->getQuery()->getSingleResult();
        $em->remove($device);
        $em->flush();
        $dm->remove($deviceDocument);
        $dm->flush();
    }
}
