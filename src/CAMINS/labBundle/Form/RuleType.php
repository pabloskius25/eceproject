<?php

namespace CAMINS\labBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use CAMINS\labBundle\Entity\Device;

class RuleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullname',null,array())
            ->add('Device', EntityType::class, array(
              'class' => 'CAMINSlabBundle:Device',
              'placeholder' => 'Select Device',
            ));

        $formModifier = function (FormInterface $form, Device $device = null) {
            $actuators = null === $device ? array() : $device->getActuators();
            $form->add('actuator', EntityType::class, array(
                'class'       => 'CAMINSlabBundle:Actuator',
                'placeholder' => 'Select Actuator',
                'choices'     => $actuators,
            ));
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();
                $formModifier($event->getForm(), $data->getDevice());
            }
        );

        $builder->get('Device')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                $device = $event->getForm()->getData();
                $formModifier($event->getForm()->getParent(), $device);
            }
        );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CAMINS\labBundle\Entity\Rule'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'camins_labbundle_rule';
    }
}
