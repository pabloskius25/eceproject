<?php

namespace CAMINS\labBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use CAMINS\labBundle\Entity\Device;

class RuleRESTType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullname',null,array())
            ->add('device',null,array())
            ->add('actuator',null,array())
            ->add('conditionals',null,array());
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CAMINS\labBundle\Entity\Rule',
            'csrf_protection' => false,
        ));
    }
    
    

    /**
     * @return string
     */
    public function getName()
    {
        return 'camins_labbundle_rule';
    }
}
