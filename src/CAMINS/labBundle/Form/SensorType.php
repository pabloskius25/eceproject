<?php

namespace CAMINS\labBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SensorType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('laboratory',null,array())
            ->add('code')
            ->add('fullname')
            ->add('description')
            ->add('structureJson')
            ->add('type')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CAMINS\labBundle\Entity\Sensor'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'camins_labbundle_sensor';
    }
}
