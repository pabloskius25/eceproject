<?php

namespace CAMINS\labBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use CAMINS\labBundle\Entity\Device;

class ConditionalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',null,array())
            ->add('device', EntityType::class, array(
              'class' => 'CAMINSlabBundle:Device',
              'placeholder' => 'Select Device',
            ))
            //->add('comparator',null,array())
            ->add('comparator',ChoiceType::class,array('choices'  => array(
				'<' => '<',
				'<=' => '<=',
				'==' => '==',
				'>=' => '>=',
				'>' => '>',
			)))
            ->add('constant',null,array())
            //->add('rule', HiddenType::class,array());
            ->add('rule', null,array('read_only' => true));
            

        $formModifier = function (FormInterface $form, Device $device = null) {
            $sensors = null === $device ? array() : $device->getSensors();
            $form->add('sensor', EntityType::class, array(
                'class'       => 'CAMINSlabBundle:Sensor',
                'placeholder' => 'Select Sensor',
                'choices'     => $sensors,
            ));
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();
                $formModifier($event->getForm(), $data->getDevice());
            }
        );

        $builder->get('device')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                $device = $event->getForm()->getData();
                $formModifier($event->getForm()->getParent(), $device);
            }
        );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CAMINS\labBundle\Entity\Conditional',
            //'csrf_protection' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'camins_labbundle_conditional';
    }
}
