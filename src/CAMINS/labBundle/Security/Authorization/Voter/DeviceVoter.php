<?php
namespace CAMINS\labBundle\Security\Authorization\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

use CAMINS\labBundle\Controller\DeviceController;
use CAMINS\labBundle\Entity\Device;
use CAMINS\labBundle\Entity\User;

class DeviceVoter extends Voter
{
    public function supports($attribute, $entity)
    {
        if (in_array($attribute, array(DeviceController::INDEX, DeviceController::CREATE))) {
            return ($entity instanceof Device);
        }

        if (in_array($attribute, array(DeviceController::VIEW, DeviceController::DOWNLOAD, DeviceController::UPDATE, DeviceController::DELETE))) {
            return ($entity instanceof Device);
        }
        return false;
    }

    protected function voteOnAttribute($attribute, $entity, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            return false;
        }
        if ($user->isAdmin()) {
            return true;
        }
        switch($attribute) {
            case DeviceController::INDEX:
            case DeviceController::DOWNLOAD:
            case DeviceController::CREATE:
                return true;
                //return ($entity->isManager($user));
            case DeviceController::VIEW:
            case DeviceController::UPDATE:
                return ($entity->isManager($user));
            case DeviceController::DELETE:
                return ($entity->isManager($user));
        }
        return false;
    }
}
