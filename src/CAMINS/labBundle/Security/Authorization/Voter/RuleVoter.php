<?php
namespace CAMINS\labBundle\Security\Authorization\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

use CAMINS\labBundle\Controller\RuleController;
use CAMINS\labBundle\Entity\Rule;
use CAMINS\labBundle\Entity\User;

class RuleVoter extends Voter
{
    public function supports($attribute, $entity)
    {
        if (in_array($attribute, array(RuleController::INDEX, RuleController::CREATE))) {
            return ($entity instanceof Rule);
        }

        if (in_array($attribute, array(RuleController::VIEW, RuleController::UPDATE, RuleController::DELETE))) {
            return ($entity instanceof Rule);
        }
        return false;
    }

    protected function voteOnAttribute($attribute, $entity, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            return false;
        }
        if ($user->isAdmin()) {
            return true;
        }
        switch($attribute) {
            case RuleController::INDEX:
            case RuleController::CREATE:
                return true;
                //return ($entity->isManager($user));
            case RuleController::VIEW:
            case RuleController::UPDATE:
                return ($entity->isManager($user));
            case RuleController::DELETE:
                return ($entity->isManager($user));
        }
        return false;
    }
}
