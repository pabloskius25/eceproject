<?php
namespace CAMINS\labBundle\Twig;

class AppExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('boolean', array($this, 'booleanFilter')),
        );
    }

    public function booleanFilter($value)
    {
        return $value ? 'yes' : 'no';
    }

    public function getName()
    {
        return 'app_extension';
    }
}
