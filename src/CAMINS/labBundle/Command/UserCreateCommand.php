<?php
namespace CAMINS\labBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use CAMINS\labBundle\Entity\User;

class UserCreateCommand extends ContainerAwareCommand
{
    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('user:create')
            ->setDescription('Create a user.')
            ->setDefinition(array(
                new InputArgument('username', InputArgument::REQUIRED, 'Username'),
                new InputArgument('password', InputArgument::REQUIRED, 'Password'),
                new InputArgument('email', InputArgument::REQUIRED, 'Email'),
                new InputArgument('displayName', InputArgument::REQUIRED, 'Display name'),
                new InputOption('admin', null, InputOption::VALUE_NONE, 'Admin'),
                new InputOption('disabled', null, InputOption::VALUE_NONE, 'Disabled'),
            ))->setHelp('user:create <username> <password> <displayName> <email> [--admin] [--disabled]');
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username    = $input->getArgument('username');
        $password    = $input->getArgument('password');
        $email       = $input->getArgument('email');
        $displayName = $input->getArgument('displayName');
        $admin       = $input->getOption('admin');
        $disabled    = $input->getOption('disabled');

        $encoder = $this->getContainer()->get('security.password_encoder');
        $service = $this->getContainer()->get('user_service');
        $user = new User();
        $user->setUsername($username);
        $user->setPassword($encoder->encodePassword($user, $password));
        $user->setEmail($email);
        $user->setDisplayName($displayName);
        $user->setAdmin($admin);
        $user->setActive(!$disabled);
        $service->createUser($user);

        $output->writeln(sprintf('Created user <comment>%s</comment>', $username));
    }
}
