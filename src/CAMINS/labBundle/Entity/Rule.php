<?php

namespace CAMINS\labBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Rule
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Rule
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="string", length=255)
     */
    private $fullname;

    /**
    * @ORM\OneToMany(targetEntity="Conditional", mappedBy="rule")
    *
    */
    private $conditionals;

    /**
    * @ORM\ManyToOne(targetEntity="Actuator", inversedBy="rules")
    * @ORM\JoinColumn(name="actuator_id", referencedColumnName="id", onDelete="CASCADE")
    */
    private $actuator;

    private $device;

    public function __construct()
    {
      $this->conditionals = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     *
     * @return Rule
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
    * Add conditional
    *
    * @param \CAMINS\labBundle\Entity\Conditional $conditional
    * @return Rule
    */
    public function addConditional(\CAMINS\labBundle\Entity\Conditional $conditional)
    {
      $this->conditionals[] = $conditional;
      return $this;
    }

    /**
    * Remove conditional
    *
    * @param \CAMINS\labBundle\Entity\Conditional $conditional
    */
    public function removeConditional(\CAMINS\labBundle\Entity\Conditional $conditional)
    {
      $this->conditionals->removeElement($conditional);
    }

    /**
    * Get conditionals
    *
    * @return \Doctrine\Common\Collections\Collection
    */
    public function getConditionals()
    {
      return $this->conditionals;
    }

    /**
     * Get device
     *
     * @return Device
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set device
     *
     * @param Device $device
     *
     * @return Rule
     */
    public function setDevice($device)
    {
        $this->device = $device;
        return $this;
    }

    /**
     * Get actuator
     *
     * @return Actuator
     */
    public function getActuator()
    {
        return $this->actuator;
    }

    /**
     * Set actuator
     *
     * @param Actuator $actuator
     *
     * @return Rule
     */
    public function setActuator($actuator)
    {
        $this->actuator = $actuator;
        return $this;
    }

    public function __toString()
    {
      return (string)$this->fullname;
    }

}
