<?php

namespace CAMINS\labBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;



/**
 * Actuator
 *
 * @JMS\ExclusionPolicy("all")
 * @ORM\Table()
 * @ORM\Entity
 */
class Actuator
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @JMS\Expose
     */
    private $name;

    /**
    * @ORM\ManyToOne(targetEntity="Device", inversedBy="actuators")
    * @ORM\JoinColumn(name="device_id", referencedColumnName="id", onDelete="CASCADE")
    */
    protected $device;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="lastValue", type="boolean")
     */
    private $lastValue = false;    

    /**
    * @ORM\OneToMany(targetEntity="Rule", mappedBy="actuator")
    *
    */
    protected $rules;

    public function __construct()
    {
      $this->rules = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
    * Set device
    *
    * @param \CAMINS\labBundle\Entity\Device $device
    * @return Actuator
    */
    public function setDevice(\CAMINS\labBundle\Entity\Device $device = null)
    {
      $this->device = $device;
      return $this;
    }

    /**
    * Get device
    *
    * @return \CAMINS\laboratoryBundle\Entity\Laboratory
    */
    public function getDevice()
    {
      return $this->device;
    }

    /**
    * Add rule
    *
    * @param \CAMINS\labBundle\Entity\Rule $rule
    * @return Device
    */
    public function addRule(\CAMINS\labBundle\Entity\Rule $rule)
    {
      $this->rules[] = $rule;
      return $this;
    }

    /**
    * Remove rule
    *
    * @param \CAMINS\labBundle\Entity\Rule $rules
    */
    public function removeRule(\CAMINS\labBundle\Entity\Rule $rule)
    {
      $this->rules->removeElement($rule);
    }

    /**
    * Get rule
    *
    * @return \Doctrine\Common\Collections\Collection
    */
    public function getRules()
    {
      return $this->rules;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Actuator
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    
    /**
     * Set lastValue
     *
     * @param boolean $lastValue
     *
     * @return Actuator
     */
    public function setLastValue($lastValue)
    {
        $this->lastValue = $lastValue;

        return $this;
    }

    /**
     * Get lastValue
     *
     * @return lastValue
     */
    public function getLastValue()
    {
        return $this->lastValue;
    }    

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString()
    {
      return (string)$this->name;
    }

}
