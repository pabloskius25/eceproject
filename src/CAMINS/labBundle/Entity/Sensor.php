<?php

namespace CAMINS\labBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;



/**
 * Sensor
 *
 * @JMS\ExclusionPolicy("all")
 * @ORM\Table()
 * @ORM\Entity
 */
class Sensor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @JMS\Expose
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="unit", type="string", length=255)
     * @JMS\Expose
     */
    private $unit;    

    /**
     * @var float
     *
     * @ORM\Column(name="lastValue", type="float", nullable=true)
     */
    private $lastValue;

    /**
    * @ORM\ManyToOne(targetEntity="Device", inversedBy="sensors")
    * @ORM\JoinColumn(name="device_id", referencedColumnName="id")
    */
    protected $device;

    /**
    * @ORM\OneToMany(targetEntity="Conditional", mappedBy="sensor")
    *
    */
    protected $conditionals;

    public function __construct()
    {
      $this->conditionals = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
    * Set device
    *
    * @param \CAMINS\labBundle\Entity\Device $device
    * @return Sensor
    */
    public function setDevice(\CAMINS\labBundle\Entity\Device $device = null)
    {
      $this->device = $device;
      return $this;
    }

    /**
    * Get device
    *
    * @return \CAMINS\laboratoryBundle\Entity\Laboratory
    */
    public function getDevice()
    {
      return $this->device;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Sensor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set unit
     *
     * @param string $unit
     *
     * @return Sensor
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
        return $this;
    }

    /**
     * Get unit
     *
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }
    
    /**
     * Set lastValue
     *
     * @param float $lastValue
     *
     * @return Sensor
     */
    public function setLastValue($lastValue)
    {
        $this->lastValue = $lastValue;

        return $this;
    }

    /**
     * Get lastValue
     *
     * @return lastValue
     */
    public function getLastValue()
    {
        return $this->lastValue;
    }
    
    /**
    * Add conditional
    *
    * @param \CAMINS\labBundle\Entity\Conditional $conditional
    * @return Sensor
    */
    public function addConditional(\CAMINS\labBundle\Entity\Conditional $conditional)
    {
      $this->conditionals[] = $conditional;
      return $this;
    }

    /**
    * Remove conditional
    *
    * @param \CAMINS\labBundle\Entity\Conditional $conditional
    */
    public function removeConditional(\CAMINS\labBundle\Entity\Conditional $conditional)
    {
      $this->conditionals->removeElement($conditional);
    }

    /**
    * Get conditionals
    *
    * @return \Doctrine\Common\Collections\Collection
    */
    public function getConditionals()
    {
      return $this->conditionals;
    }    

    public function __toString()
    {
      return (string)$this->name ." (".(string)$this->unit.")" ;
    }

}
