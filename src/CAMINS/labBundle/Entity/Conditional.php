<?php

namespace CAMINS\labBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Conditional
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Conditional
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="comparator", type="string", length=255)
     */
    private $comparator;

    /**
     * @var float
     *
     * @ORM\Column(name="constant", type="float")
     */
    private $constant;

    /**
    * @ORM\ManyToOne(targetEntity="Sensor", inversedBy="conditionals")
    * @ORM\JoinColumn(name="sensor_id", referencedColumnName="id", onDelete="CASCADE")
    */
    private $sensor;

    /**
    * @ORM\ManyToOne(targetEntity="Rule", inversedBy="conditionals")
    * @ORM\JoinColumn(name="rule_id", referencedColumnName="id", onDelete="CASCADE")
    */
    protected $rule;

    protected $device;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Conditional
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set comparator
     *
     * @param string $comparator
     *
     * @return Conditional
     */
    public function setComparator($comparator)
    {
        $this->comparator = $comparator;

        return $this;
    }

    /**
     * Get comparator
     *
     * @return string
     */
    public function getComparator()
    {
        return $this->comparator;
    }

    /**
     * Set constant
     *
     * @param float $constant
     *
     * @return Conditional
     */
    public function setConstant($constant)
    {
        $this->constant = $constant;

        return $this;
    }

    /**
     * Get constant
     *
     * @return float
     */
    public function getConstant()
    {
        return $this->constant;
    }

    /**
     * Set sensor
     *
     * @param Sensor $sensor
     *
     * @return Conditional
     */
    public function setSensor($sensor)
    {
        $this->sensor = $sensor;
        return $this;
    }

    /**
     * Get sensor
     *
     * @return Sensor
     */
    public function getSensor()
    {
        return $this->sensor;
    }

    /**
     * Set rule
     *
     * @param Rule $rule
     *
     * @return Conditional
     */
    public function setRule($rule)
    {
        $this->rule = $rule;
        return $this;
    }

    /**
     * Get rule
     *
     * @return Rule
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * Get device
     *
     * @return Device
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set device
     *
     * @param Device $device
     *
     * @return Conditional
     */
    public function setDevice($device)
    {
        $this->device = $device;
        return $this;
    }

    public function __toString()
    {
      return (string)$this->name;
    }

}
