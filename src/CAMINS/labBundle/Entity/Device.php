<?php

namespace CAMINS\labBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Device
 * @JMS\ExclusionPolicy("all")
 * @ORM\Table()
 * @ORM\Entity
 */
class Device
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="string", length=255)
     * @JMS\Expose
     */
    private $fullname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createDateTime", type="datetime")
     */
    private $createDateTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_time", type="datetime")
     */
    private $updateTime;

    /**
    * Temporary variable to store ReadingDocument data.
    */
    public $tempData;

    /**
    * @ORM\ManyToOne(targetEntity="User", inversedBy="devices")
    * @ORM\JoinColumn(name="user_manager", referencedColumnName="id", nullable=true)
    */
    protected $manager;

    /**
    * @ORM\OneToMany(targetEntity="Sensor", mappedBy="device")
	* @ORM\JoinColumn(onDelete="CASCADE")
    * @JMS\Expose
    */
    protected $sensors;

    /**
    * @ORM\OneToMany(targetEntity="Actuator", mappedBy="device")
    * @ORM\JoinColumn(onDelete="CASCADE")
    * @JMS\Expose
    */
    protected $actuators;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255)
     */
    private $token;

    protected $temporarySensors;
    protected $temporaryActuators;

    public function __construct()
    {
      $this->sensors = new ArrayCollection();
      $this->actuators = new ArrayCollection();
    }

     /**
      * Set manager
      *
      * @param CAMINS\labBundle\Entity\User $usuaris
      * @return Laboratory
      */
      public function setManager(User $user)
      {
          $this->manager = $user;
          return $this;
      }

      /**
      * Get manager
      *
      * @return CAMINS\labBundle\Entity\User
      */
      public function getManager()
      {
          return $this->manager;
      }

      /**
      * Get if $user is the manager
      *
      * @param CAMINS\labBundle\Entity\User $user
      * @return boolean
      */
      public function isManager(User $user)
      {
          return $this->getManager() == $user;
      }

     /**
      * Set fullname
      *
      * @param string $name
      *
      * @return Device
      */
     public function setFullname($fullname)
     {
         $this->fullname = $fullname;
         return $this;
     }

     /**
      * Get fullname
      *
      * @return string
      */
     public function getFullname()
     {
         return $this->fullname;
     }

     /**
      * Set temporarySensors
      *
      * @param string $temporarySensors
      *
      * @return Device
      */
     public function setTemporarySensors($temporarySensors)
     {
         $this->temporarySensors = $temporarySensors;
         return $this;
     }

     /**
      * Get temporarySensors
      *
      * @return string
      */
     public function getTemporarySensors()
     {
         return $this->temporarySensors;
     }

     /**
      * Set temporaryActuators
      *
      * @param string $temporaryActuators
      *
      * @return Device
      */
     public function setTemporaryActuators($temporaryActuators)
     {
         $this->temporaryActuators = $temporaryActuators;
         return $this;
     }

     /**
      * Get temporaryActuators
      *
      * @return string
      */
     public function getTemporaryActuators()
     {
         return $this->temporaryActuators;
     }

    /**
    * Add actuator
    *
    * @param \CAMINS\labBundle\Entity\Actuator $actuator
    * @return Device
    */
    public function addActuator(\CAMINS\labBundle\Entity\Actuator $actuator)
    {
      $this->actuators[] = $actuator;
      return $this;
    }

    /**
    * Remove actuator
    *
    * @param \CAMINS\labBundle\Entity\Actuator $actuator
    */
    public function removeActuator(Actuator $actuator)
    {
      $this->actuators->removeElement($actuator);
    }

    /**
    * Get actuators
    *
    * @return \Doctrine\Common\Collections\Collection
    */
    public function getActuators()
    {
      return $this->actuators;
    }

    /**
    * Add rule
    *
    * @param \CAMINS\labBundle\Entity\Rule $rule
    * @return Device
    */
    public function addRule(\CAMINS\labBundle\Entity\Rule $rule)
    {
      $this->rules[] = $rule;
      return $this;
    }

    /**
    * Remove rule
    *
    * @param \CAMINS\labBundle\Entity\Rule $rule
    */
    public function removeRule(\CAMINS\labBundle\Entity\Rule $rule)
    {
      $this->rules->removeElement($rule);
    }

    /**
    * Add sensor
    *
    * @param \CAMINS\labBundle\Entity\Sensor $sensor
    * @return Device
    */
    public function addSensor(\CAMINS\labBundle\Entity\Sensor $sensor)
    {
      $this->sensors[] = $sensor;
      return $this;
    }

    /**
    * Remove sensor
    *
    * @param \CAMINS\labBundle\Entity\Sensor $sensor
    */
    public function removeSensor(Sensor $sensor)
    {
      $this->sensors->removeElement($sensor);
    }

    /**
    * Delete all sensor and actuators
    *
    */
    public function deleteAllSensorActuator()
    {
      $this->sensors = new ArrayCollection();
      $this->actuators = new ArrayCollection();      
    }

    /**
    * Get sensors
    *
    * @return \Doctrine\Common\Collections\Collection
    */
    public function getSensors()
    {
      return $this->sensors;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createDateTime
     *
     * @param \DateTime $createDateTime
     *
     * @return Device
     */
    public function setCreateDateTime($createDateTime)
    {
        $this->createDateTime = $createDateTime;
        return $this;
    }

    /**
     * Get createDateTime
     *
     * @return \DateTime
     */
    public function getCreateDateTime()
    {
        return $this->createDateTime;
    }

    /**
     * Set updateTime
     *
     * @param \DateTime $updateTime
     *
     * @return Reading
     */
    public function setUpdateTime($updateTime)
    {
        $this->updateTime = $updateTime;
        return $this;
    }

    /**
     * Get updateTime
     *
     * @return \DateTime
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Reading
     */
    public function setToken($googleToken)
    {
        $this->token = $googleToken;
        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    public function __toString()
    {
      return (string)$this->fullname;
    }

}
